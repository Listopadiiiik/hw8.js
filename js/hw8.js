
// Теоретичні питаня

// 1. Це модель HTML документу, через яку підключається javascript до HTML.

// 2. innerHTML повертає тег та текст, тоді як innerText повертає тільки текст.

// 3. через getElementBy**(''), querySelector('') та querySelectorAll(''). 
// Але це залежить від ситуації. Якщо нам треба до конкретного елементу звернутись, використовуємо
// перший та другий методи. Якщо до всіх єлементів одного класу - третім методом.

// Завдання

let para = Array.from(document.getElementsByTagName('p'));
alert("This document contains " + para.length + ' paragraphs');
para.forEach(el => el.style.cssText = 'background-color: #ff0000;');
console.log(para);

let opt = document.getElementById('optionsList');
console.log(opt);
console.log(opt.parentNode);
console.log('');
console.log(opt.childNodes);
console.log(''); 

let content = document.getElementById('testParagraph')
content.innerText = 'This is a paragraph';
console.log(content);

let main = document.querySelectorAll('.main-header > *, .main-header > * > *, .main-header > * > * > *, .main-header > * > * > * > *');
main.forEach(function(item){
    item.classList.add('nav-item');
})
console.log(main);

let section = Array.from(document.querySelectorAll('.section-title'));
section.forEach(item => item.classList.remove('section-title'));
console.log(section);